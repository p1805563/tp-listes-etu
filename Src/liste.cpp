#include "liste.hpp"

#include <iostream>
#include <cassert>

Liste::Liste() {
  elem = nullptr;
  tailleTab = 0;
}

Liste::Liste(const Liste& autre)
  : elem(nullptr), tailleTab(0)
{
    *this = autre;
}

Liste& Liste::operator=(const Liste& autre) {
  if(this != &autre) {
    delete elem;
    tailleTab = 0;
    elem = nullptr;
    if(autre.elem) {
      Cellule* tmp = autre.elem;
      while(tmp != nullptr){
        ajouter_en_queue(tmp->value);
        tmp = tmp->nextC;
      }
    }
  }
  return *this;
}

Liste::~Liste() {
  delete elem;
}

void Liste::ajouter_en_tete(int valeur) {
  elem = new Cellule(valeur,elem);
  tailleTab++;
}

void Liste::ajouter_en_queue(int valeur) {
  if(!elem)
    elem = new Cellule(valeur,elem);
  else {
    Cellule* tmp = elem;
    while(tmp->nextC) {
      tmp = tmp->nextC;
    }
    tmp->nextC = new Cellule(valeur,nullptr);
  }
  tailleTab++;
}

void Liste::supprimer_en_tete() {
  if(elem != nullptr) {
    tailleTab--;
    Cellule* tmp = elem->nextC;
    elem->nextC = nullptr;
    delete elem;
    elem = tmp;
  }
}

Cellule* Liste::tete() {
  if(elem == nullptr)
    return nullptr ;
  else
    return elem;
}

const Cellule* Liste::tete() const {
  if(elem == nullptr)
    return nullptr ;
  else
    return elem;
}

Cellule* Liste::queue() {
  if(!elem)
    return nullptr ;
  else {
    Cellule* tmp = elem;
    while(tmp->nextC) {
      tmp = tmp->nextC;
    }
    return tmp;
  }
}

const Cellule* Liste::queue() const {
  if(!elem)
    return nullptr ;
  else {
    Cellule* tmp = elem;
    while(tmp->nextC) {
      tmp = tmp->nextC;
    }
    return tmp;
  }
}

int Liste::taille() const {
  return tailleTab;
}

Cellule* Liste::recherche(int valeur) {
  if(!elem)
    return nullptr ;
  else {
    Cellule* tmp = elem;
    while(tmp->value != valeur) {
      if(tmp->nextC)
        tmp = tmp->nextC;
      else
        return nullptr;
    }
    return tmp;
  }
}

const Cellule* Liste::recherche(int valeur) const {
  if(!elem)
    return nullptr ;
  else {
    Cellule* tmp = elem;
    while(tmp->value != valeur) {
      if(tmp->nextC)
        tmp = tmp->nextC;
      else
        return nullptr;
    }
    return tmp;
  }
}

void Liste::afficher() const {
  Cellule* tmp = elem;
  std::cout << "[ ";
  for(int i = 0; i < tailleTab; i++) {
    std::cout << tmp->value << " ";
    tmp = tmp->nextC;
  }
  std::cout << "]" << std::endl;
}
