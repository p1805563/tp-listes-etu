#ifndef LIFAP6_LISTE_CELLULE_HPP
#define LIFAP6_LISTE_CELLULE_HPP

class Cellule {
  public :
    Cellule();
    Cellule(int valeur,Cellule* cell);
    ~Cellule();

    int value;
    Cellule* nextC;
} ;

#endif
