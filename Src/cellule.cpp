#include "cellule.hpp"

Cellule::Cellule() {
  value = 0;
  nextC  = nullptr;

}

Cellule::Cellule(int valeur, Cellule* cell) {
  value = valeur;
  nextC = cell;
}

Cellule::~Cellule() {
  if(nextC != nullptr)
    delete nextC;
}
